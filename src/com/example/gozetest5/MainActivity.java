package com.example.gozetest5;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;


public class MainActivity extends FragmentActivity 
{

	ViewPager viewPager;

	private BottomFilterViewPager mAdapter;

	private ViewPager mPager;

	private static final String MSG_1 = "Food is very delicious here , grate cook !"  ;
	private static final String MSG_2 = "i realy loved the service ... they were very fast"  ;
	private static final String MSG_3 = "you must try desert number 4 in the menu !!!!"  ;



	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mAdapter = new BottomFilterViewPager(getSupportFragmentManager());
		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(mAdapter);



	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
