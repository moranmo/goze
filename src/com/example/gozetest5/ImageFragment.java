package com.example.gozetest5;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

@SuppressLint("ValidFragment")
public class ImageFragment extends Fragment {
	private final int imageResourceId;
	
//	
//	 public static void init(int val) {
////	        ImageFragment truitonFrag = new ImageFragment();
//	        // Supply val input as an argument.
//	        Bundle args = new Bundle();
////	        args.putInt("val", val);
////	        truitonFrag.setArguments(args);
//	        return truitonFrag;
//	    }
	

	public ImageFragment(int imageResourceId) {
		this.imageResourceId = imageResourceId;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e("Test", "hello");
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(imageResourceId, container, false);
		
//		View view = inflater.inflate(R.layout.bottom_sweep_filter, container, false);
//		View view = inflater.inflate(R.layout., container, false);
//		ImageView imageView = (ImageView) view.findViewById(R.id.choice1);
//		imageView.setImageResource(imageResourceId);
		return view;
	}
}


